#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>

#include "md5.h"

const int PASS_LEN=50;        // Maximum any password can be
const int HASH_LEN=33;        // Length of MD5 hash strings

// Given a hash and a plaintext guess, return 1 if
// the hash of the guess matches the given hash.
// That is, return 1 if the guess is correct.
int tryguess(char *hash, char *guess)
{
    char *hashedGuess;
    // Hash the guess using MD5
    hashedGuess = md5(guess, strlen(guess));
    // Compare the two hashes
    int result = strcmp(hash, hashedGuess);

    // Free any malloc'd memory
    free(hashedGuess);
    
    if (result == 0)
        return 1;
    else
        return 0;
    
    return 0;
}

char **readfile(char *filename)
{
    //malloc space for entire file
    //get size of file
    struct stat st;
    if(stat(filename, &st) == -1)
    {
        fprintf(stderr, "Can't get info about %s\n", filename);
        exit(1);
    }
    int len = st.st_size;
    char *file = malloc(len); //unsigned char is raw bytes , char is characters //##################################
    //read entire file into memory
    FILE *f = fopen(filename, "r");
    
    if(!f)
    {
        printf("Can't open %s for read \n", filename);
        exit(1);
    }
    fread(file, 1, len, f); //
    fclose(f);
    //replace \n with \0
    int count = 0;
    
    for(int i =0; i<len; i++)
    {
        if(file[i] == '\n')
        {
            file[i] = '\0';
            count ++;
        }
    }
    //malloc space for array of pointers
    char **line = malloc((count+1) * sizeof(char*)); //characters to pointers//####################################
    
    //fill in addresses
    int word = 0;
    line[word] = file; //the first word in the fill
    word++;
    for (int i = 1; i<len; i++)
    {
        if (file[i] == '\0' && i+1 < len)
        {
            line[word] = &file[i+1];
            word++;
        }
    }
    line[word] = NULL;
    //return address of second array
    return line;
}

// TODO
// Read in the hash file and return the array of strings.
char **read_hashes(char *filename)
{
    char **hashes = readfile(filename);
    
    if (hashes)
        return hashes;
    
    return NULL;
}


// TODO
// Read in the dictionary file and return the data structure.
// Each entry should contain both the hash and the dictionary
// word.
char **read_dict(char *filename)
{
    return NULL;
}


int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    // TODO: Read the hash file into an array of strings
    char **hashes = read_hashes(argv[1]);

    // TODO: Read the dictionary file into an array of strings
    char **dict = read_dict(argv[2]);
    
    // TODO: Sort the hashed dictionary using qsort.
    // You will need to provide a comparison function.
    qsort(dict, ___, ___, ___);

    // TODO
    // For each hash, search for it in the dictionary using
    // binary search.
    // If you find it, get the corresponding plaintext dictionary
    // entry. Print both the hash and word out.
    // Need only one loop. (Yay!)
}
